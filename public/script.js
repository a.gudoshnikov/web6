
function getResult() {
    let type = document.getElementById("type").value;
    let prices = {
        default: [100, 200, 300],
        "2": {
            options: {
                "option1": 10,
                "option2": 20,
                "option3": 30
            }
        },
        "3": {
            properties: {
                "prop1": 100,
                "prop2": 200
            }
        }
    };

    let optionDiv = document.getElementById("options");
    if (type === "2") {
        optionDiv.style.display = "block";
    } else {
        optionDiv.style.display = "none";
    }


    let checkDiv = document.getElementById("checkboxes");
    if (type === "3") {
        checkDiv.style.display = "block";
    } else {
        checkDiv.style.display = "none";
    }

    let price = prices.default[0];

    if (type === "2") {
        price = prices.default[1];
        let options = document.querySelectorAll("#options input");
        options.forEach(function (option) {
            if (option.checked) {
                price += prices["2"].options[option.value];
            }
        });
    }

    if (type === "3") {
        price = prices.default[2];
        let properties = document.querySelectorAll("#checkboxes input");
        properties.forEach(function (property) {
            if (property.checked) {
                price += prices["3"].properties[property.name];
            }
        });
    }
    let result = document.getElementById("result");
    let quantity = document.getElementById("quantity").value;
    let Result = quantity * price;

    if (Number.isNaN(Result)) {
        let line = "Введите подходящие значения";
        document.getElementById("result").innerHTML = line;
    } else {
        result.innerHTML = "Result is " + Result;
    }
}

window.addEventListener("DOMContentLoaded", function () {
    let optionDiv = document.getElementById("options");
    optionDiv.style.display = "none";
    let checkDiv = document.getElementById("checkboxes");
    checkDiv.style.display = "none";

    let select = document.getElementById("type");
    select.addEventListener("change", getResult);

    let options = document.querySelectorAll("#options input");
    options.forEach(function (option) {
        option.addEventListener("change", getResult);
    });

    let checkboxes = document.querySelectorAll("#checkboxes input");
    checkboxes.forEach(function (checkbox) {
        checkbox.addEventListener("change", getResult);
    });

    let quantity = document.getElementById("quantity");
    quantity.addEventListener("input", getResult);

    getResult();
});